import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  apiUrl = environment.apiUrl;
  appId = environment.appId;

  constructor(private http: HttpClient) { }

  getCurrentWeatherData(city) {
    return this.http.get(`${this.apiUrl}weather?q=${city}&units=metric&appid=${this.appId}`);
  }

  getWeatherForecast(city): Observable<any> {
    return this.http.get(`${this.apiUrl}forecast?q=${city}&units=metric&appid=${this.appId}`);
  }
}
