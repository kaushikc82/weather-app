import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  citiesData = [];

  constructor(private wetherService: WeatherService) { }

  ngOnInit(): void {
    this.getCitiesData();
  }

  getCitiesData() {
    combineLatest([
      this.wetherService.getCurrentWeatherData('paris'),
      this.wetherService.getCurrentWeatherData('rome'),
      this.wetherService.getCurrentWeatherData('berlin'),
      this.wetherService.getCurrentWeatherData('copenhagen'),
      this.wetherService.getCurrentWeatherData('manchester')
    ])
      .subscribe((res) => {
        this.citiesData = res;
        this.citiesData.forEach(data => {
          data.main.temp = Math.round(data.main.temp);
        });
      }, error => {
        alert('Error occured ' + error);
      });
  }

}
