import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {
  cityName;
  forecastData = [];

  constructor(private route: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.cityName = this.route.snapshot.paramMap.get('city');
    this.getForecast();
  }

  getForecast() {
    this.weatherService.getWeatherForecast(this.cityName)
        .subscribe((res) => {
          this.forecastData = res.list;
          this.forecastData.forEach(data => {
            data.main.temp = Math.round(data.main.temp);
          });
          this.forecastData = this.forecastData.filter((val) => val.dt_txt.split(' ')[1] === '09:00:00');
        }, error => {
          alert(error.error.message);
        });
  }

}
